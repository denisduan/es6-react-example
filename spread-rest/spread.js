
//----------------------------------------------------------------------
// Passing values/arguments into function call - spread

function print(x, y, x) {
    console.log(x, y, x);
}

print(...[2, 3, 'Hello']);



var a = [2, 3];
var b = [4, 5, 6];
var c = [1, ...a, ...b, 7];
console.log(c);



//----------------------------------------------------------------------
// It act as "gather" operation in function signature - gather 

function gatherPrint(a, b, ...c) {
    console.log(`a: ${a}`)
    console.log(`b: ${b}`);
    console.log(`c: ${c}`);
}

gatherPrint(1, 2, 3, 4, 5, 6, 7, 8);


//----------------------------------------------------------------------
// Spreading object

var style = {
    color : 'red',
    width : '100%',
    height : '500px',
}

var props = {
    style : {
        opacity: '1',
        textAlign: 'center',
        fontSize: '20px',
    }
}

var compoenentStyle = {...style, ...props.style};

console.log(compoenentStyle);

// Warning 
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_operator#AutoCompatibilityTable

