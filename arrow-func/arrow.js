/**
 * ------------------------------------------------
 * Basic comparision 
 * ------------------------------------------------
 */

var foo = (x, y) => x + y;

// VS

function foo(x, y) {
    return x + y;
}

/**
 * The body only needs to be enclosed by { .. } if there's more than one expression, 
 * or if the body consists of a non-expression statement.
 */

// Different variation

var f1 = () => 12;

var f2 = x => x * 2;

var f3 = (x, y) => {
    var z = x * 2 + y;
    y++;
    x *= 3;
    return (x + y + z) / 2;
};




/**
 * ------------------------------------------------
 * "this" binding behavior
 * ------------------------------------------------
 */

// Problem Code
var msg = 'Save the world!';

function showMsg() {
    console.log(this.msg);
}

showMsg();

//var button = {};
//button.onClick = showMsg;
//button.onClick();


// Solution One - closure
button.onClick = function () {
    showMsg();
}



// Solution Two - bind()
// Ref MDN: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_objects/Function/bind
button.onClick = showMsg.bind(this);



// Solution Three - Arrow Function
var showMsg = () => console.log(msg);
