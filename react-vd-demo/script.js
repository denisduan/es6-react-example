
// Container for native javascript code
const jsContainer = document.getElementById("js");

// Container for react js code
const reactContainer = document.getElementById("react");

const render = () => {

  jsContainer.innerHTML = `
    <div class="demo">
      Hello JS
      <p>${new Date()}</p>
    </div>
  `;

  /**
   http://babeljs.io/ 
   
   const element = (
    <div clasName="demo">
      Hello React
      <input />
      <p> {new Date().toString()} </p>
    </div>
  );
  **/

  ReactDOM.render(
    React.createElement(
      "div", {
        className: "demo"
      },
      "Hello React",
      React.createElement(
        "p", 
        null,
        " ", 
        new Date().toString(),
        " "
      )
    ),
    reactContainer
  );

}

setInterval(render, 1000);