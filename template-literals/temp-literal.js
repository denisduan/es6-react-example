
/** Pre ES6 **/

var name = 'Denis';
var msg = 'Hello ' + name + '\n' + 'He can do math: 2 power of 2 is ' + (2 + 2);

//console.log(msg); 


/** ES6 Template Literal **/
console.log(

`Hello ${name}
He can do math: 2 power of 2 is ${Math.pow(2, 2)}
`
);