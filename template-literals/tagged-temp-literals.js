

// Tagged template literals
 

function foo(strings, ...values) {
	console.log( strings );
	console.log( values );
}

var name = 'denis';

foo`Hello ${name}
He can do math: 2 power of 2 is ${Math.pow(2, 2)}
`




//----------------------------------------------------------------------

// Pass through tag function

function passThrough(strings, ...values) {
	
	var str = ''
	
	for (i = 0; i < strings.length; i++) {
	 
	    if (i > 0) {
	        str += values[i - 1];
	    }
	 
	    str += strings[i];
	}

	return str;
}

console.log(passThrough`Hello ${name}
He can do math: 2 power of 2 is ${Math.pow(2, 2)}`);




//----------------------------------------------------------------------
// To uppper value tagged template literals

function toUpperStrValues(strings, ...values) {
	
	var str = ''
	
	for (i = 0; i < strings.length; i++) {

	    if (i > 0) {

	        str += (typeof values[i - 1] === 'string') ? values[i - 1].toUpperCase() : values[i - 1];
	        
	    }

	    str += strings[i];
	}

	return str;
}

console.log( 
toUpperStrValues`Hello ${name}
He can do math: 2 power of 2 is ${Math.pow(2, 2)}` 
);